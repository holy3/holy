<?php 
/**
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class C_Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login');
	}

	public function index(){
		$data = array(
            "title" => "Login Admin"
        );
        if ($this->session->userdata('isLoginAdmin') == TRUE) {
            redirect('dashboard/admin');
        }else{
            $this->load->view('V_Login',$data);
        }
	}

    function logout(){
        $this->session->sess_destroy();
        redirect('home');
    }
	public function auth(){
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('V_Login');
        }else{
            $usr    = $this->input->post('username');
            $psw    = md5($this->input->post('password'));
            $cek    = $this->M_Login->cek($usr,$psw);
            if($cek->num_rows() != 0){
                foreach ($cek->result() as $dat){
                    $sess_data['isLoginAdmin']      = TRUE;
					$sess_data['id']                = $dat->id_user;
                    $sess_data['username']          = $dat->username;
                    $sess_data['hak_akses']         = $dat->hak_akses;
                    $this->session->set_userdata($sess_data);
                }                
                $this->session->set_userdata($sess_data);
                if ($this->session->userdata('hak_akses') == "admin") {
                    redirect('admin/akun');
                }else if($this->session->userdata('hak_akses') == "direksi"){
                    redirect('dashboard/admin');
                }else if($this->session->userdata('hak_akses') == "keuangan"){
                    redirect('admin/keuangan');
                }else{
                    redirect('kusioner');
                }
            }else {
                $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
                redirect('admin/');
            }
        }
    }
}
 ?>