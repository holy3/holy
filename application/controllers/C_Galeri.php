<?php 
	/**
	 * 
	 */
	class C_Galeri extends CI_Controller
	{
		
		function __construct()
		{	
			parent::__construct();
            $this->load->model('M_Data_Galeri');
			$this->load->helper('url');
		}

		function index(){
		$data['galeri'] = $this->M_Data_Galeri->tampil_data()->result();
		$this->load->view('Back/V_Galeri',$data);
		}

		function tambah(){
		$this->load->view('Back/V_Add_Galeri');
		}

		function edit($id_paket){
		$id_galeri = $this->uri->segment(4);
		$where = array('id_galeri' => $id_galeri);
        $data['galeri'] = $this->M_Data_Galeri->edit_data($where,'galeri')->result();
		$this->load->view('Back/V_Edit_Galeri',$data);
		}

		function tambah_aksi(){
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 0;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('userfile')){
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload', $error);
			}else{
				$_data = array('upload_data' => $this->upload->data());
				 $data = array(
					'nama'=> $this->input->post('nama'),
					'deskripsi'=> $this->input->post('deskripsi'),
					'gambar' => $_data['upload_data']['file_name']
					);
				$query = $this->db->insert('galeri',$data);
				if($query){
					redirect('admin/galeri');
				}else{
					redirect('admin/galeri/add');
				}
			}
		}

		function update(){
        $id_galeri = $this->input->post('id_galeri');     
        $nama = $this->input->post('nama');
        $deskripsi = $this->input->post('deskripsi');

		if(empty($_FILES['userfile'])){
			$data = array(
				'nama'=> $this->input->post('nama'),
				'deskripsi'=> $this->input->post('deskripsi'),
				'gambar' => $this->input->post('gambar_lama')
				);
			$where = array (
				'id_galeri' => $id_galeri
			);
			$this->M_Data_Galeri->update_data($where,$data,'galeri');
			redirect('admin/galeri');
		}else{
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'gif|jpg|png';
				$config['max_size']             = 0;
				$config['max_width']            = 0;
				$config['max_height']           = 0;
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('userfile')){
						$error = array('error' => $this->upload->display_errors());
						redirect('admin/galeri/edit/'.$id_galeri, $error);
				}else{
					$_data = array('upload_data' => $this->upload->data());
					 $data = array(
						'nama'=> $this->input->post('nama'),
						'deskripsi'=> $this->input->post('deskripsi'),
						'gambar' => $_data['upload_data']['file_name']
						);
					$where = array (
						'id_galeri' => $id_galeri
					);
					$query = $this->M_Data_Galeri->update_data($where,$data,'galeri');
					redirect('admin/galeri');
				}	
		}
			
		}

		function hapus($id){
		$id_galeri = $this->uri->segment(4);
		$where = array('id_galeri' => $id_galeri);
		$this->M_Data_Galeri->hapus_data($where,'galeri');
		redirect('admin/galeri');
		}


	}
 ?>