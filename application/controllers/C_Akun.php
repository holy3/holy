<?php 
	/**
	 * 
	 */
	class C_Akun extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('M_Data_Akun');
			$this->load->helper('url');
		}

		function index(){
			$data['akun'] = $this->M_Data_Akun->tampil_data()->result();
			$data['page'] = "Account";

			$this->load->view('Back/Parts/V_Header');
			$this->load->view('Back/Parts/V_Navigation',$data);

			$this->load->view('Back/akun/V_Akun',$data);
			
			$this->load->view('Back/Parts/V_Footer');
		}

		function create(){
			$data['kode'] = $this->M_Data_Akun->make_id();
			$data['page'] = "Create Account";

			$this->load->view('Back/Parts/V_Header');
			$this->load->view('Back/Parts/V_Navigation',$data);

			$this->load->view('Back/akun/V_Create',$data);	

			$this->load->view('Back/Parts/V_Footer');
		}

		function edit($id_user){
			$id_user = $this->uri->segment(4);
			$where = array('id_user' => $id_user);
			$data['akun'] = $this->M_Data_Akun->edit_data($where,'user')->result();
			$data['page'] = "Edit Account";
			
			$this->load->view('Back/Parts/V_Header');
			$this->load->view('Back/Parts/V_Navigation',$data);

			$this->load->view('Back/akun/V_Edit',$data);	

			$this->load->view('Back/Parts/V_Footer');
		}

		function tambah_aksi(){
			$id_user = $this->input->post('id_user');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$name = $first_name." ".$last_name;
			$password = md5($this->input->post('password'));

			$data = array(
				'id_user' => $id_user,
				'username' => $username,
				'name' => $name,
				'email' => $email,
				'password' => $password,
				'hak_akses' => "user",
				);
			
			$this->M_Data_Akun->input_data($data,'user');

			$this->session->set_flashdata('message', 'success');

			redirect('admin/account');
		}

		function update(){
			$id_user = $this->input->post('id_user');
			$username = $this->input->post('username');
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$name = $first_name." ".$last_name;
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$password_lama = $this->input->post('password_lama');
			$hak_akses = $this->input->post('hak_akses');

			if ($password == $password_lama) {
				$data = array(
				'username' => $username,
				'name' => $name,
				'email' => $email,
				'hak_akses' => $hak_akses
				);
			$where = array(
				'id_user' => $id_user,
			);
			$this->M_Data_Akun->update_data($where,$data,'user');
			redirect('admin/account');
			}else{
				$data = array(
				'username' => $username,
				'name' => $name,
				'email' => $email,
				'password' => $password,
				'hak_akses' => $hak_akses
				);
			$where = array(
				'id_user' => $id_user,
			);
			$this->M_Data_Akun->update_data($where,$data,'user');

			$this->session->set_flashdata('message', 'updated');

			redirect('admin/account');
		}

		
		}

		function hapus($id){
			$id_user = $this->uri->segment(3);
			$where = array('id_user' => $id_user);
			$this->M_Data_Akun->hapus_data($where,'user');

			$this->session->set_flashdata('message', 'deleted');

			redirect('admin/account');
		}


	}
 ?>