<?php 
/**
 * 
 */
class C_Front extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Home');
	}

	public function index(){
		$data = array(
			"title" 		=> "Home"
		);
		$this->load->view('Front/V_Home',$data);
	}
	public function home(){
		$data = array(
			"title" 		=> "Home"
		);
		$this->load->view('Front/V_Home',$data);    }
    
	public function about(){
		$this->load->view('Front/V_About');
    }
    
    public function shop(){
		$data = array(
			"title" 		=> "Shop"
		);
        $this->load->view('Front/V_Shop',$data);
    }

    public function shop_detail(){
		$data = array(
			"title" 		=> "Detail"
		);
		if ($this->uri->segment(2)=="1"){
			$this->load->view('Front/V_Shop_Detail',$data);
		}elseif ($this->uri->segment(2)=="2"){
			$this->load->view('Front/V_Shop_Detail2',$data);
		}elseif ($this->uri->segment(2)=="3"){
			$this->load->view('Front/V_Shop_Detail3',$data);
		}else{
			$this->load->view('Front/V_Shop_Detail',$data);
		}
    }
    
    public function contact(){
		$this->load->view('Front/V_Contact');
    }
}
 ?>
