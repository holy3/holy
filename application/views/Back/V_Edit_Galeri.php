<?php
$this->load->view("Back/Parts/V_Header");
$this->load->view("Back/Parts/V_Navigation");
?>

    <div class="content-body">
            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo base_url('C_Galeri/update') ?>" method="post" enctype="multipart/form-data">
                                        <?php foreach ($galeri as $i) { ?>
                                            <input type="hidden" name="id_galeri" value="<?php echo $i->id_galeri ?>">
                                            <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-nama">Nama <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="nama" required="" value="<?php echo $i->nama ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-email">Deskripsi <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control" name="deskripsi" required=""><?php echo $i->deskripsi ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-userfile">Upload Gambar <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <span>Preview</span> <br>
                                                <img src="<?php echo base_url('uploads/'.$i->gambar)?>" alt="" width="50px"> 
                                                <hr>
                                                <input type="hidden" name="gambar_lama" value="<?php echo $i->gambar ?>">
                                                <input type="file" class="form-control" name="userfile" id="userfile">
                                            </div>
                                        </div>
                                        <?php } ?>
                                    
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <a href="<?php echo base_url() ?>admin/galeri"><button type="button" class="btn btn-danger">
                                                  Cancel
                                                </button></a>
                                                <button type="submit" id="btnSubmit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
<?php 
  $this->load->view("Back/Parts/V_Footer");
 ?>