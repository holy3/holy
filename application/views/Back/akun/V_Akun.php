
      <div class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                    <a style="float: right;" href="<?php echo base_url('admin/account/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                  <h4 class="card-title ">Data Account</h4>
                  <p class="card-category" style="float: left;"> User</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class="text-primary">
                        <th>No.</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th class="text-center">Action</th>
                      </thead>
                      <tbody>
                        <?php 
                            $i = 1;
                            foreach ($akun as $k) { 
                        ?>
                            <tr>
                              <td><?php echo $i++ ?></td>
                              <td><?php echo $k->name ?></td>
                              <td><?php echo $k->username ?></td>
                              <td><?php echo $k->email ?></td>
                              <td class="text-center"><a href="<?php echo base_url('admin/account/edit/'.$k->id_user) ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                              <button data-target="#myModal-<?php echo $i ?>"  data-toggle="modal" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                            </tr>
                            <div id="myModal-<?php echo $i ?>" class="modal fade">
                            <div class="modal-dialog modal-confirm">
                              <div class="modal-content"  style="background-color: #131827; color: white;">
                                <div class="modal-header flex-column">
                                  <div class="icon-box">
                                    <i class="material-icons">&#xE5CD;</i>
                                  </div>						
                                  <h4 class="modal-title w-100">Are you sure?</h4>	
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <p>Do you really want to delete <?php echo $k->name ?>? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer justify-content-center">
                                  <button type="button"  class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                  <a type="button" href="<?php echo base_url('C_Akun/hapus/'.$k->id_user) ?>" class="btn btn-danger">Delete</a>
                                </div>
                              </div>
                            </div>
                          </div> 
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>      
        </div>
      </div>
      