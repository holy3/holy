      <div class="content">
        <div class="container-fluid">
          <div class="row">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Form Account</h4>
                  <p class="card-category">Complete your profile</p>
                </div>
                <div class="card-body">
                  <?php foreach ($akun as $k) { ?>
                  <form action="<?php echo base_url('C_Akun/update/'.$k->id_user) ?>" method="post">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="hidden" name="id_user" value="<?php echo $k->id_user ?>">
                          <input type="text" class="form-control" name="username" value="<?php echo $k->username ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input type="email" class="form-control" name="email" value="<?php echo $k->email ?>" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" class="form-control" name="first_name" value="<?php echo $k->name ?>" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" class="form-control" name="last_name" value="<?php echo $k->name ?>" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control" name="password" value="<?php echo $k->password ?>" required>
                          <input type="hidden" class="form-control" name="password_lama" value="<?php echo $k->password ?>" required>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Update Account</button>
                    <div class="clearfix"></div>
                  </form>
                  <?php } ?>
                </div>
            </div>
          </div>
        </div>
      </div>


     