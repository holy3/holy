<?php error_reporting(E_ALL^E_NOTICE)?>

<body class="dark-edition">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="black" data-image="<?php echo base_url() ?>assets/images/banner2.png">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
        <a href="" class="simple-text logo-normal">
        <img src="<?php echo base_url() ?>assets/images/logo.png" width="50" alt="">

        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?php if($this->uri->segment(2) == "dashboard"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/dashboard">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(2) == "account"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/account">
              <i class="fa fa-user"></i>
              <p>Account</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(2) == "product"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/product">
              <i class="fa fa-shopping-bag"></i>
              <p>Product</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(2) == "dashboard-user"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/dashboard-user">
              <i class="fa fa-dashboard"></i>
              <p>Manage Dashboard User</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(2) == "galery"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/galery">
              <i class="fa fa-photo"></i>
              <p>Galery</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(2) == "report"){ echo "active"; } ?>">
            <a class="nav-link" href="<?php echo base_url() ?>admin/report">
              <i class="fa fa-bar-chart"></i>
              <p>Report</p>
            </a>
          </li>
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo base_url() ?>admin/dashboard"><?php echo $page; ?></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)">
                  <i class="material-icons">notifications</i>
                  <p class="d-lg-none d-md-block">
                    Notifications
                  </p>
                </a>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
