<?php
$this->load->view('Front/Parts/V_Header');
$this->load->view('Front/Parts/V_Navigation');
?>
<!-- Shop Details Section Begin -->
<section class="shop-details">
	<div class="product__details__pic">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="product__details__breadcrumb">
						<a href="<?php echo base_url('home')?>">Home</a>
						<a href="<?php echo base_url('shop')?>">Shop</a>
						<span>Product Details</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-1.JPG">
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#tabs-2" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-2.JPG">
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-3.JPG">
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-4.JPG">
								</div>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-5.JPG">
								</div>
							</a>
						</li>
<!--						for video-->
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab">
								<div class="product__thumb__pic set-bg"
									 data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-sq.png">
									<i class="fa fa-play"></i>
								</div>
							</a>
						</li>

					</ul>
				</div>
				<div class="col-lg-6 col-md-9">
					<div class="tab-content">
						<div class="tab-pane active" id="tabs-1" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-1.png" alt="">
							</div>
						</div>
						<div class="tab-pane" id="tabs-2" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-2.png" alt="">
							</div>
						</div>
						<div class="tab-pane" id="tabs-3" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-3.png" alt="">
							</div>
						</div>
						<div class="tab-pane" id="tabs-4" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-4.png" alt="">
							</div>
						</div>
						<div class="tab-pane" id="tabs-5" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-5.png" alt="">
							</div>
						</div>

<!--						for video-->
						<div class="tab-pane" id="tabs-6" role="tabpanel">
							<div class="product__details__pic__item">
								<img src="<?php echo base_url()?>assets/images/baju/mino-black-sq.png" alt="">
								<a href="https://www.youtube.com/watch?v=6W6HhdqA95w" class="video-popup"><i class="fa fa-play"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="product__details__content">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-8">
					<div class="product__details__text">
						<h4>Minorities Black</h4>
<!--						<div class="rating">-->
<!--							<i class="fa fa-star"></i>-->
<!--							<i class="fa fa-star"></i>-->
<!--							<i class="fa fa-star"></i>-->
<!--							<i class="fa fa-star"></i>-->
<!--							<i class="fa fa-star-o"></i>-->
<!--							<span> - 5 Reviews</span>-->
<!--						</div>-->
						<h3>IDR. 125.000 </h3>
						<p>see the size chart below.</p>
						<div class="product__details__option">
							<div class="product__details__option__size">
								<span>Size:</span>
								<label for="xl">xl
									<input type="radio" id="xl">
								</label>
								<label class="active" for="l">l
									<input type="radio" id="l">
								</label>
								<label for="sm">m
									<input type="radio" id="sm">
								</label>
							</div>
							<div class="product__details__option__color">
								<span>Color:</span>
								<label class="c-1" for="sp-1">
									<input type="radio" id="sp-1">
								</label>
							</div>
						</div>

<!--						<div class="product__details__cart__option">-->
<!--							<div class="quantity">-->
<!--								<div class="pro-qty">-->
<!--									<input type="text" value="1">-->
<!--								</div>-->
<!--							</div>-->
<!--							<a href="#" class="primary-btn">add to cart</a>-->
<!--						</div>-->

						<div class="product__details__btns__option">
							<a target="_blank" href="https://www.tokopedia.com/theholytrinity/minorities-black">
								<img style="max-width: 100px" src="<?php echo base_url()?>assets/images/btn-tokped.png" alt=""><span>Tokopedia</span>
							</a>
							<a target="_blank" href="https://shopee.co.id/theholytrinity">
								<img style="max-width: 100px" src="<?php echo base_url()?>assets/images/btn-shopee-o.png" alt=""><span>Shopee</span>
							</a>
						</div>

<!--						<div class="product__details__last__option">-->
<!--							<h5><span>Guaranteed Safe Checkout</span></h5>-->
<!--							<img src="--><?php //echo base_url()?><!--assets/front/img/shop-details/details-payment.png" alt="">-->
<!--							<ul>-->
<!--								<li><span>SKU:</span> 3812912</li>-->
<!--								<li><span>Categories:</span> Clothes</li>-->
<!--								<li><span>Tag:</span> Clothes, Skin, Body</li>-->
<!--							</ul>-->
<!--						</div>-->

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="product__details__tab">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#tabs-7"
								   role="tab">Description</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tabs-8" role="tab">Size Chart</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tabs-9" role="tab">Additional
									information</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tabs-7" role="tabpanel">
								<div class="product__details__tab__content">
									<div class="product__details__tab__content__item">
										<h5>Products Infomation</h5>
										<p>A Pocket PC is a handheld computer, which features many of the same
											capabilities as a modern PC. These handy little devices allow
											individuals to retrieve and store e-mail messages, create a contact
											file, coordinate appointments, surf the internet, exchange text messages
											and more. Every product that is labeled as a Pocket PC must be
											accompanied with specific software to operate the unit and must feature
											a touchscreen and touchpad.</p>
										<p>As is the case with any new technology product, the cost of a Pocket PC
											was substantial during it’s early release. For approximately $700.00,
											consumers could purchase one of top-of-the-line Pocket PCs in 2003.
											These days, customers are finding that prices have become much more
											reasonable now that the newness is wearing off. For approximately
											$350.00, a new Pocket PC can now be purchased.</p>
									</div>
									<div class="product__details__tab__content__item">
										<h5>Material used</h5>
										<p>Polyester is deemed lower quality due to its none natural quality’s. Made
											from synthetic materials, not natural like wool. Polyester suits become
											creased easily and are known for not being breathable. Polyester suits
											tend to have a shine to them compared to wool and cotton suits, this can
											make the suit look cheap. The texture of velvet is luxurious and
											breathable. Velvet is a great choice for dinner party jacket and can be
											worn all year round.</p>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tabs-8" role="tabpanel">
								<div class="product__details__tab__content">
									<div class="product__details__tab__content__item">
										<img style="max-width: 500px; display: block;margin: 0 auto;"
											 src="<?php echo base_url()?>assets/images/standard.png" alt="size chart">
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tabs-9" role="tabpanel">
								<div class="product__details__tab__content">
									<div class="product__details__tab__content__item">
										<h5></h5>
										<p>A minority group, by its original definition, refers to a group of people whose practices,
											race, religion, ethnicity, or other characteristics are lesser in numbers than the main groups of
											those classifications.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Shop Details Section End -->

<!-- Related Section Begin -->
<section class="related spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="related-title">Related Product</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6 col-sm-6">
				<div class="product__item">
					<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/lilac-sq2.png">
						<ul class="product__hover">
							<li>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-lilac">
									<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('')?>shop-detail/2">
									<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="product__item__text">
						<h6>Authentic Lilac</h6>
						<a href="<?php echo base_url('')?>shop-detail/2" class="add-cart">+ Detail</a>
						<div class="rating">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
						</div>
						<h5>IDR. 125.000</h5>
						<div class="product__color__select">
							<label class="active lilac" for="pc-5">
								<input type="radio" id="pc-5">
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-sm-6">
				<div class="product__item">
					<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/green-sq.png">
						<ul class="product__hover">
							<li>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-limegreen">
									<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('')?>shop-detail/3">
									<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="product__item__text">
						<h6>Authentic Green</h6>
						<a href="<?php echo base_url('')?>shop-detail/3" class="add-cart">+ Detail</a>
						<div class="rating">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
						</div>
						<h5>IDR. 125.000</h5>
						<div class="product__color__select">
							<label class="active green" for="pc-5">
								<input type="radio" id="pc-5">
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Related Section End -->
<?php
$this->load->view('Front/Parts/V_Footer');
?>
