
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3">
				<div class="header__logo">
					<a href="<?php echo base_url()?>home"><img src="<?php echo base_url()?>assets/images/logo-small.png" alt="tht"></a>
					<b>The Holy Trinity</b>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<nav class="header__menu mobile-menu">
					<ul>
						<li <?php if ($this->uri->segment(1)=="home"){echo "class='active'";}?>><a href="<?php echo base_url()?>home">Home</a></li>
						<li <?php if ($this->uri->segment(1)=="shop" || $this->uri->segment(1)=="shop-detail"){echo "class='active'";}?>>
							<a href="<?php echo base_url()?>shop">Shop</a></li>
<!--						<li><a href="#">Pages</a>-->
<!--							<ul class="dropdown">-->
<!--								<li><a href="--><?php //echo base_url()?><!--assets/front/about.html">About Us</a></li>-->
<!--								<li><a href="--><?php //echo base_url()?><!--assets/front/shop-details.html">Shop Details</a></li>-->
<!--								<li><a href="--><?php //echo base_url()?><!--assets/front/shopping-cart.html">Shopping Cart</a></li>-->
<!--								<li><a href="--><?php //echo base_url()?><!--assets/front/checkout.html">Check Out</a></li>-->
<!--								<li><a href="--><?php //echo base_url()?><!--assets/front/blog-details.html">Blog Details</a></li>-->
<!--							</ul>-->
<!--						</li>-->
<!--						<li><a href="--><?php //echo base_url()?><!--assets/front/blog.html">Blog</a></li>-->
<!--						<li><a href="--><?php //echo base_url()?><!--assets/front/contact.html">Contacts</a></li>-->
					</ul>
				</nav>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="header__nav__option">
					<a href="#" class="search-switch"><img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""></a>
<!--					<a href="#"><img src="--><?php //echo base_url()?><!--assets/front/img/icon/heart.png" alt=""></a>-->
<!--					<a href="#"><img src="--><?php //echo base_url()?><!--assets/front/img/icon/cart.png" alt=""> <span>0</span></a>-->
<!--					<div class="price">$0.00</div>-->
				</div>
			</div>
		</div>
		<div class="canvas__open"><i class="fa fa-bars"></i></div>
	</div>
</header>
<!-- Header Section End -->
<?php
if($this->uri->segment(1)!="home" || $this->uri->segment(1) === false){
?>

<?php
}
