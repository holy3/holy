
<!-- Footer Section Begin -->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="footer__about">
					<div class="footer__logo">
						<a href="#"><img src="<?php echo base_url()?>assets/images/logo.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
				<div class="footer__widget">
					<h6>Navigation</h6>
					<ul>
						<li><a href="<?php echo base_url('home')?>">Home</a></li>
						<li><a href="<?php echo base_url('shop')?>">Shop</a></li>
					</ul>
				</div>

			</div>
			<div class="col-lg-2 col-md-3 col-sm-6">
				<div class="footer__widget">
					<h6>Our Thought</h6>
					<div class="footer__newslatter">
						<p>The customer is at the heart of our unique business model, which includes design.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
				<div class="footer__widget">
					<h6>NewLetter</h6>
					<div class="footer__newslatter">
						<p>Be the first to know about new arrivals, look books, sales & promos!</p>
						<form action="#">
							<input type="text" placeholder="Your email">
							<button type="submit"><span class="icon_mail_alt"></span></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="footer__copyright__text">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					<p>Copyright ©
						<script>
							document.write(new Date().getFullYear());
						</script>
						All rights reserved
					</p>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Footer Section End -->

<!-- Search Begin -->
<div class="search-model">
	<div class="h-100 d-flex align-items-center justify-content-center">
		<div class="search-close-switch">+</div>
		<form class="search-model-form">
			<input type="text" id="search-input" placeholder="Search here.....">
		</form>
	</div>
</div>
<!-- Search End -->

<!-- Js Plugins -->
<script src="<?php echo base_url()?>assets/front/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/jquery.slicknav.js"></script>
<script src="<?php echo base_url()?>assets/front/js/mixitup.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url()?>assets/front/js/main.js"></script>
</body>

</html>
