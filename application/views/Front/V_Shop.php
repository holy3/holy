<?php
$this->load->view('Front/Parts/V_Header');
$this->load->view('Front/Parts/V_Navigation');
?>
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-option">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="breadcrumb__text">
					<h4>Shop</h4>
					<div class="breadcrumb__links">
						<a href="<?php echo base_url('home')?>">Home</a>
						<span><?php echo $title?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Breadcrumb Section End -->
<!-- Shop Section Begin -->
<section class="shop spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="shop__sidebar">
					<div class="shop__sidebar__search">
						<form action="#">
							<input type="text" placeholder="Search...">
							<button type="submit"><span class="icon_search"></span></button>
						</form>
					</div>
					<div class="shop__sidebar__accordion">
						<div class="accordion" id="accordionExample">
							<div class="card">
								<div class="card-heading">
									<a data-toggle="collapse" data-target="#collapseOne">Categories</a>
								</div>
								<div id="collapseOne" class="collapse show" data-parent="#accordionExample">
									<div class="card-body">
										<div class="shop__sidebar__categories">
											<ul class="nice-scroll">
												<li><a href="#">Men</a></li>
												<li><a href="#">Women</a></li>
												<li><a href="#">Bags</a></li>
												<li><a href="#">Clothing</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Kids</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
<!--							<div class="card">-->
<!--								<div class="card-heading">-->
<!--									<a data-toggle="collapse" data-target="#collapseTwo">Branding</a>-->
<!--								</div>-->
<!--								<div id="collapseTwo" class="collapse show" data-parent="#accordionExample">-->
<!--									<div class="card-body">-->
<!--										<div class="shop__sidebar__brand">-->
<!--											<ul>-->
<!--												<li><a href="#">Louis Vuitton</a></li>-->
<!--												<li><a href="#">Chanel</a></li>-->
<!--												<li><a href="#">Hermes</a></li>-->
<!--												<li><a href="#">Gucci</a></li>-->
<!--											</ul>-->
<!--										</div>-->
<!--									</div>-->
<!--								</div>-->
<!--							</div>-->
							<div class="card">
								<div class="card-heading">
									<a data-toggle="collapse" data-target="#collapseThree">Filter Price</a>
								</div>
								<div id="collapseThree" class="collapse show" data-parent="#accordionExample">
									<div class="card-body">
										<div class="shop__sidebar__price">
<!--											<ul>-->
<!--												<li><a href="#">$0.00 - $50.00</a></li>-->
<!--												<li><a href="#">$50.00 - $100.00</a></li>-->
<!--												<li><a href="#">$100.00 - $150.00</a></li>-->
<!--												<li><a href="#">$150.00 - $200.00</a></li>-->
<!--												<li><a href="#">$200.00 - $250.00</a></li>-->
<!--												<li><a href="#">250.00+</a></li>-->
<!--											</ul>-->
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-heading">
									<a data-toggle="collapse" data-target="#collapseFour">Size</a>
								</div>
								<div id="collapseFour" class="collapse show" data-parent="#accordionExample">
									<div class="card-body">
										<div class="shop__sidebar__size">
											<label for="xs">s
												<input type="radio" id="xs">
											</label>
											<label for="sm">m
												<input type="radio" id="sm">
											</label>
											<label for="md">l
												<input type="radio" id="md">
											</label>
											<label for="xl">xl
												<input type="radio" id="xl">
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-heading">
									<a data-toggle="collapse" data-target="#collapseFive">Colors</a>
								</div>
								<div id="collapseFive" class="collapse show" data-parent="#accordionExample">
									<div class="card-body">
										<div class="shop__sidebar__color">
											<label class="c-1" for="sp-1">
												<input type="radio" id="sp-1">
											</label>
											<label class="c-green" for="sp-2">
												<input type="radio" id="sp-2">
											</label>
											<label class="c-lilac" for="sp-3">
												<input type="radio" id="sp-3">
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-heading">
									<a data-toggle="collapse" data-target="#collapseSix">Tags</a>
								</div>
								<div id="collapseSix" class="collapse show" data-parent="#accordionExample">
									<div class="card-body">
										<div class="shop__sidebar__tags">
											<a href="#">Product</a>
<!--											<a href="#">Bags</a>-->
<!--											<a href="#">Shoes</a>-->
											<a href="#">Fashion</a>
											<a href="#">Clothing</a>
<!--											<a href="#">Hats</a>-->
<!--											<a href="#">Accessories</a>-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="shop__product__option">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="shop__product__option__left">
								<p>Showing 1–3 of 3 results</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="shop__product__option__right">
								<p>Sort by Price:</p>
								<select disabled>
									<option value="">Low To High</option>
									<option value="">IDR. 0 - IDR. 125.000</option>
									<option value="">IDR. 125.000+</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="product__item">
							<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-sq.png">
								<ul class="product__hover">
									<li>
										<a target="_blank" href="https://www.tokopedia.com/theholytrinity/minorities-black">
											<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
										</a>
									</li>
									<li>
										<a target="_blank" href="https://shopee.co.id/theholytrinity">
											<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('')?>shop-detail/1">
											<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="product__item__text">
								<h6>Minorities Black</h6>
								<a href="<?php echo base_url('')?>shop-detail/1" class="add-cart">+ Detail</a>
								<div class="rating">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
								<h5>IDR. 125.000</h5>
								<div class="product__color__select">
									<label class="active black" for="pc-5">
										<input type="radio" id="pc-5">
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="product__item">
							<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/lilac-sq2.png">
								<ul class="product__hover">
									<li>
										<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-lilac">
											<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
										</a>
									</li>
									<li>
										<a target="_blank" href="https://shopee.co.id/theholytrinity">
											<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('')?>shop-detail/2">
											<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="product__item__text">
								<h6>Authentic Lilac</h6>
								<a href="<?php echo base_url('')?>shop-detail/2" class="add-cart">+ Detail</a>
								<div class="rating">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
								<h5>IDR. 125.000</h5>
								<div class="product__color__select lilac">
									<label class="active lilac" for="pc-5">
										<input type="radio" id="pc-5">
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="product__item">
							<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/green-sq.png">
								<ul class="product__hover">
									<li>
										<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-limegreen">
											<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
										</a>
									</li>
									<li>
										<a target="_blank" href="https://shopee.co.id/theholytrinity">
											<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('')?>shop-detail/3">
											<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="product__item__text">
								<h6>Authentic Green</h6>
								<a href="<?php echo base_url('')?>shop-detail/3" class="add-cart">+ Detail</a>
								<div class="rating">
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
									<i class="fa fa-star-o"></i>
								</div>
								<h5>IDR. 125.000</h5>
								<div class="product__color__select green">
									<label class="active green" for="pc-5">
										<input type="radio" id="pc-5">
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="product__pagination">
							<a class="active" href="#">1</a>
<!--							<a href="#">2</a>-->
<!--							<a href="#">3</a>-->
<!--							<span>...</span>-->
<!--							<a href="#">21</a>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Shop Section End -->
<?php
$this->load->view('Front/Parts/V_Footer');
?>
