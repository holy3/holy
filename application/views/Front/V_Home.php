<?php
$this->load->view('Front/Parts/V_Header');
$this->load->view('Front/Parts/V_Navigation');
?>
<!-- Hero Section Begin -->
<section class="hero">
	<div class="hero__slider owl-carousel">
		<div class="hero__items set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/ban1.png">
			<div class="container">
				<div class="row">
					<div class="col-xl-5 col-lg-7 col-md-8">
						<div class="hero__text">
							<h6>The Holy Trinity</h6>
							<h2>New Collections 2021</h2>
							<p>Ethically crafted with an unwavering <br>
								commitment to exceptional quality.</p>
							<a href="<?php echo base_url()?>shop" class="primary-btn">Shop now <span class="arrow_right"></span></a>
							<div class="hero__social">
								<a target="_blank" href="https://www.instagram.com/theholytrinity.co/"><i class="fa fa-instagram"></i></a>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img style="max-width: 15px; margin-top: 10px" src="<?php echo base_url()?>assets/images/icon-shopee.png" alt="">
								</a>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity">
									<img style="max-width: 15px; margin-top: 10px" src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt="">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="hero__items set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/ban2.png">
			<div class="container">
				<div class="row">
					<div class="col-xl-5 col-lg-7 col-md-8">
						<div class="hero__text">
							<h6>The Holy Trinity</h6>
							<h2>New Collections 2021</h2>
							<p>Ethically crafted with an unwavering <br>
								commitment to exceptional quality.</p>
							<a href="<?php echo base_url()?>shop" class="primary-btn">Shop now <span class="arrow_right"></span></a>
							<div class="hero__social">
								<a target="_blank" href="https://www.instagram.com/theholytrinity.co/"><i class="fa fa-instagram"></i></a>
								<a target="_blank" href="https://shopee.co.id/theholytrinity"><img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""></a>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity"><img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Hero Section End -->

<!-- Banner Section Begin -->
<section class="banner spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 offset-lg-4">
				<div class="banner__item">
					<div class="banner__item__pic">
						<img style="max-width: 440px" src="<?php echo base_url()?>assets/images/baju/mino-black-5-sq.png" alt="">
					</div>
					<div class="banner__item__text">
						<h2>Minorities Black</h2>
						<a href="<?php echo base_url()?>shop-detail/1">Shop now</a>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="banner__item banner__item--middle">
					<div class="banner__item__pic">
						<img style="max-width: 440px" src="<?php echo base_url()?>assets/images/baju/green-3-sq.png" alt="">
					</div>
					<div class="banner__item__text">
						<h2>Authentic Green</h2>
						<a href="<?php echo base_url()?>shop-detail/3">Shop now</a>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="banner__item banner__item--last">
					<div class="banner__item__pic">
						<img style="max-width: 440px" src="<?php echo base_url()?>assets/images/baju/lilac-3-sq.png" alt="">
					</div>
					<div class="banner__item__text">
						<h2>Authentic Lilac</h2>
						<a href="<?php echo base_url()?>shop-detail/2">Shop now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Banner Section End -->

<!-- Product Section Begin -->
<section class="product spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="filter__controls">
					<li class="active" data-filter="*">Best Sellers</li>
					<li data-filter=".new-arrivals">New Arrivals</li>
					<li data-filter=".hot-sales">Hot Sales</li>
				</ul>
			</div>
		</div>
		<div class="row product__filter">
			<div class="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 mix new-arrivals">
				<div class="product__item">
					<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/mino-black-sq.png">
						<span class="label">New</span>
						<ul class="product__hover">
							<li>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity/minorities-black">
									<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('')?>shop-detail/1">
									<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="product__item__text">
						<h6>Minorities Black</h6>
						<a href="<?php echo base_url('')?>shop-detail/1" class="add-cart">+ Detail</a>
						<div class="rating">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
						</div>
						<h5>IDR. 125.000</h5>
						<div class="product__color__select">
							<label class="active black" for="pc-5">
								<input type="radio" id="pc-5">
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 mix new-arrivals">
				<div class="product__item">
					<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/lilac-sq2.png">
						<span class="label">New</span>
						<ul class="product__hover">
							<li>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-lilac">
									<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('')?>shop-detail/2">
									<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="product__item__text">
						<h6>Authentic Lilac</h6>
						<a href="<?php echo base_url('')?>shop-detail/2" class="add-cart">+ Detail</a>
						<div class="rating">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
						</div>
						<h5>IDR. 125.000</h5>
						<div class="product__color__select">
							<label class="active purple" for="pc-5">
								<input type="radio" id="pc-5">
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-6 col-md-6 col-sm-6 mix new-arrivals">
				<div class="product__item">
					<div class="product__item__pic set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/green-sq.png">
						<span class="label">New</span>
						<ul class="product__hover">
							<li>
								<a target="_blank" href="https://www.tokopedia.com/theholytrinity/oversized-authentic-limegreen">
									<img src="<?php echo base_url()?>assets/images/icon-tokopedia.png" alt=""><span>Tokopedia</span>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://shopee.co.id/theholytrinity">
									<img src="<?php echo base_url()?>assets/images/icon-shopee.png" alt=""><span>Shopee</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('')?>shop-detail/3">
									<img src="<?php echo base_url()?>assets/front/img/icon/search.png" alt=""><span>Detail</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="product__item__text">
						<h6>Authentic Green</h6>
						<a href="<?php echo base_url('')?>shop-detail/3" class="add-cart">+ Detail</a>
						<div class="rating">
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
							<i class="fa fa-star-o"></i>
						</div>
						<h5>IDR. 125.000</h5>
						<div class="product__color__select">
							<label class="active green" for="pc-5">
								<input type="radio" id="pc-5">
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Product Section End -->

<!-- Categories Section Begin -->
<section class="categories spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="categories__text">
					<h2>Free <br /> <span>Accessories</span> <br /> Bag</h2>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="categories__hot__deal">
					<img src="<?php echo base_url()?>assets/images/free-shadow.png" alt="">
					<div class="hot__deal__sticker">
						<span>FREE</span>
						<h5>For You</h5>
					</div>
				</div>
			</div>
			<div class="col-lg-4 offset-lg-1">
				<div class="categories__deal__countdown">
					<span>free with every t-shirt purchase</span>
					<h2>Only for the first 50 buyers</h2>
					<a href="<?php echo base_url('shop')?>" class="primary-btn">Shop now</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Categories Section End -->

<!-- Instagram Section Begin -->
<section class="instagram spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="instagram__pic">
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/square/SRS_4434.jpg"></div>
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/kaos4.JPG"></div>
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/kaos3.JPG"></div>
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/kaos2.JPG"></div>
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/baju/kaos1.JPG"></div>
					<div class="instagram__pic__item set-bg" data-setbg="<?php echo base_url()?>assets/images/square/ig4.jpeg"></div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="instagram__text">
					<h2>Social Media</h2>
					<a target="_blank" href="https://www.instagram.com/theholytrinity.co/">
						<img style="max-width: 100px" src="<?php echo base_url()?>assets/images/ig.png" alt="">
					</a>
<!--					<a target="_blank" href="https://www.twitter.com/theholytrinity.co/">-->
<!--						<img style="max-width: 130px" src="--><?php //echo base_url()?><!--assets/images/twitter.png" alt="">-->
<!--					</a>-->
					<p></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Instagram Section End -->

<!-- Latest Blog Section Begin -->
<section class="latest spad">
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			<div class="col-lg-12">-->
<!--				<div class="section-title">-->
<!--					<span>Latest News</span>-->
<!--					<h2>Fashion New Trends</h2>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--		<div class="row">-->
<!--			<div class="col-lg-4 col-md-6 col-sm-6">-->
<!--				<div class="blog__item">-->
<!--					<div class="blog__item__pic set-bg" data-setbg="--><?php //echo base_url()?><!--assets/front/img/blog/blog-1.jpg"></div>-->
<!--					<div class="blog__item__text">-->
<!--						<span><img src="--><?php //echo base_url()?><!--assets/front/img/icon/calendar.png" alt=""> 16 February 2020</span>-->
<!--						<h5>What Curling Irons Are The Best Ones</h5>-->
<!--						<a href="#">Read More</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-4 col-md-6 col-sm-6">-->
<!--				<div class="blog__item">-->
<!--					<div class="blog__item__pic set-bg" data-setbg="--><?php //echo base_url()?><!--assets/front/img/blog/blog-2.jpg"></div>-->
<!--					<div class="blog__item__text">-->
<!--						<span><img src="--><?php //echo base_url()?><!--assets/front/img/icon/calendar.png" alt=""> 21 February 2020</span>-->
<!--						<h5>Eternity Bands Do Last Forever</h5>-->
<!--						<a href="#">Read More</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="col-lg-4 col-md-6 col-sm-6">-->
<!--				<div class="blog__item">-->
<!--					<div class="blog__item__pic set-bg" data-setbg="--><?php //echo base_url()?><!--assets/front/img/blog/blog-3.jpg"></div>-->
<!--					<div class="blog__item__text">-->
<!--						<span><img src="--><?php //echo base_url()?><!--assets/front/img/icon/calendar.png" alt=""> 28 February 2020</span>-->
<!--						<h5>The Health Benefits Of Sunglasses</h5>-->
<!--						<a href="#">Read More</a>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
</section>
<!-- Latest Blog Section End -->
<?php
$this->load->view('Front/Parts/V_Footer');
?>
