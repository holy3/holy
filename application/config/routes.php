<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'C_Front/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// LOGIN
$route['login']                 			= 'C_Login';
$route['auth']                  			= 'C_Login/auth';
$route['admin/auth']            			= 'C_login/auth';
$route['auth/(:any)']						= 'C_Login/auth/';
$route['admin/auth/(:any)']					= 'C_Login/auth/';
$route['logout']          					= 'C_Login/logout';

// FRONT
$route['home']                 				= 'C_Front/home';
$route['shop']                 				= 'C_Front/shop';
$route['shop-detail']                 		= 'C_Front/shop_detail';
$route['shop-detail/(:any)']				= 'C_Front/shop_detail';


// BACK
$route['admin']                 			= 'C_Back/dashboard';
$route['admin/dashboard']       			= 'C_Back/dashboard';
//Data Account
$route['admin/account']                     = 'C_Akun';
$route['admin/account/create']              = 'C_Akun/create';
$route['admin/account/edit/(:any)']         = 'C_Akun/edit/';
//Data Dashboard User
$route['admin/dashboard-user']              = 'C_User';
$route['admin/dashboard-user/create']       = 'C_User/create';
$route['admin/dashboard-user/edit/(:any)']  = 'C_User/edit/';
//Data Galery
$route['admin/galery']                      = 'C_Galery';
$route['admin/galery/create']               = 'C_Galery/create';
$route['admin/galery/edit/(:any)']          = 'C_Galery/edit/';
//Data Product
$route['admin/product']                     = 'C_Product';
$route['admin/product/create']              = 'C_Product/create';
$route['admin/product/edit/(:any)']         = 'C_Product/edit/';


